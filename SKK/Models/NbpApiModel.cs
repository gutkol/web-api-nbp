﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SKK.Models
{
    public class NbpApiModel
    {
        [JsonProperty("data")]
        public string Date { get; set; }
        [JsonProperty("cena")]
        public double Price { get; set; }
    }
}