﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using SKK.Models;
using Newtonsoft.Json;

namespace SKK.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetGoldPrice(string dateFrom, string dateTo)
        {
            List<NbpApiModel> apiModel = new List<NbpApiModel>();

            using (var httpClient = new HttpClient())
            {
                string url = String.Format("http://api.nbp.pl/api/cenyzlota/{0}/{1}/?format=json", dateFrom, dateTo);

                //var json = httpClient.GetStringAsync(url).Result;

                //apiModel = JsonConvert.DeserializeObject<List<NbpApiModel>>(json);

                HttpResponseMessage Res = httpClient.GetAsync(url).Result;

                if (Res.IsSuccessStatusCode)
                {
                    var json = Res.Content.ReadAsStringAsync().Result;
                    apiModel = JsonConvert.DeserializeObject<List<NbpApiModel>>(json);
                }

                return new JsonResult { Data = apiModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

    }
}