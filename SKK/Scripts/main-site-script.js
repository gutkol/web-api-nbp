﻿$(document).ready(function () {
    var gold = [];

    $('#datepicker-group-1').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        clearBtn: true,
        language: 'pl'
    });

    $('#datepicker-group-2').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        clearBtn: true,
        language: 'pl'
    });

    $('#getGoldPriceButton').click(function () {
        gold = [];

        var dateFrom = $('#date-group-1').val();
        var dateTo = $('#date-group-2').val();

        if (dateTo < dateFrom) {
            $('#warning-date').show();
            return false;
        }

        $.ajax({
            type: 'GET',
            url: '/Home/GetGoldPrice',
            data: { dateFrom: dateFrom, dateTo: dateTo },
            success: function (data) {
                if (data) {
                    $.each(data, function (index, value) {
                        gold.push({
                            Date: value.Date,
                            Price: value.Price
                        });
                    });

                    $('#result').css("display", "block");
                    $('#warning-date').hide();

                    generateGoldTable(gold);
                }
            }
        });
    });


    var goldTableBody = null;
    var goldTableInfo = null;

    var tableToSort = [];

    function generateGoldTable(gold) {
        goldTableBody = "";
        goldTableInfo = "";
        tableToSort = [];

        if (gold.length !== 0) {
            for (var i = 0; i < gold.length; i++) {
                goldTableBody += '<tr><td>';
                goldTableBody += gold[i].Date + '</td>';
                goldTableBody += '<td>' + gold[i].Price.toFixed(2) + '</td>';
                goldTableBody += '</tr>';

                tableToSort[i] = gold[i];
            }

            sortGoldValuesASC(tableToSort);
            var minGoldValue = tableToSort[0];
            var maxGoldValue = tableToSort[tableToSort.length - 1];
            var medianGoldValue;

            var middleIndex = Math.floor(tableToSort.length / 2);

            if (tableToSort.length % 2 === 0) {
                medianGoldValue = (tableToSort[middleIndex - 1].Price + tableToSort[middleIndex].Price) / 2;
            }
            else {
                medianGoldValue = tableToSort[middleIndex].Price;
            }

            goldTableInfo = '<p> Min wartość złota: ' + minGoldValue.Price + ' Data: ' + minGoldValue.Date + '</p>';
            goldTableInfo += '<p> Max wartość złota: ' + maxGoldValue.Price + ' Data: ' + maxGoldValue.Date + '</p>';
            goldTableInfo += '<p> Mediana wartości złota: ' + medianGoldValue + '</p>';
        }
        else {
            goldTableInfo = '<p>Brak danych</p>';
        }

        $('#gold-table-body').html(goldTableBody);
        $('#gold-table-info').html(goldTableInfo);
    }


    function sortGoldValuesASC(tableToSort) {
        tableToSort.sort(function (x, y) {
            var n = x.Price - y.Price;
            if (n !== 0) {
                return n;
            }

            return x.Date - y.Date;
        });
    }


    $('#sortradio1').click(function () {
        generateGoldTable(gold);
    });

    $('#sortradio2').click(function () {
        var descDateGold = [];
        var temp = 0;

        for (var i = gold.length - 1; i >= 0; i--) {
            descDateGold[temp] = gold[i];
            temp++;
        }

        generateGoldTable(descDateGold);
    });

    $('#sortradio3').click(function () {
        var ascValueGold = [];

        for (var i = gold.length - 1; i >= 0; i--) {
            ascValueGold[i] = gold[i];
        }

        ascValueGold.sort(function (x, y) {
            var n = x.Price - y.Price;
            if (n !== 0) {
                return n;
            }

            return x.Date - y.Date;
        });

        generateGoldTable(ascValueGold);
    });

    $('#sortradio4').click(function () {
        var descValueGold = [];

        for (var i = gold.length - 1; i >= 0; i--) {
            descValueGold[i] = gold[i];
        }

        descValueGold.sort(function (x, y) {
            var n = y.Price - x.Price;
            if (n !== 0) {
                return n;
            }

            return y.Date - x.Date;
        });

        generateGoldTable(descValueGold);
    });

});